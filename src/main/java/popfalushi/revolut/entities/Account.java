package popfalushi.revolut.entities;

import io.netty.util.internal.StringUtil;

/**
 *
 */
public class Account {
    private String guid;
    private long amount;

    public Account(String guid, long amount) {
        if(StringUtil.isNullOrEmpty(guid)){
            throw new IllegalArgumentException();
        }
        this.guid = guid;
        this.amount = amount;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Account{" +
                "guid='" + guid + '\'' +
                ", amount=" + amount +
                '}';
    }
}
