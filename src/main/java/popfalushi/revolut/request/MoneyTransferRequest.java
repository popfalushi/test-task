package popfalushi.revolut.request;

import io.netty.util.internal.StringUtil;

/**
 *
 */
public class MoneyTransferRequest {

    public MoneyTransferRequest(String accountFrom, String accountTo, long amount) {
        if(StringUtil.isNullOrEmpty(accountFrom) || StringUtil.isNullOrEmpty(accountTo) || amount <= 0){
            throw new RequestValidationException("Cannot create MoneyTransferRequest instance. " +
                    "accountFrom=" + accountFrom + " accountTo=" + accountTo + " amount=" + amount);
        }
        this.accountFrom = accountFrom;
        this.accountTo = accountTo;
        this.amount = amount;
    }

    private String accountFrom;
    private String accountTo;
    private long amount;

    public String getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(String accountFrom) {
        this.accountFrom = accountFrom;
    }

    public String getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(String accountTo) {
        this.accountTo = accountTo;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "MoneyTransferRequest{" +
                "accountFrom='" + accountFrom + '\'' +
                ", accountTo='" + accountTo + '\'' +
                ", amount=" + amount +
                '}';
    }
}
