package popfalushi.revolut.request;

/**
 *
 */
public class RequestValidationException extends IllegalArgumentException {
    public RequestValidationException() {
        super();
    }

    public RequestValidationException(String s) {
        super(s);
    }

    public RequestValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestValidationException(Throwable cause) {
        super(cause);
    }
}
