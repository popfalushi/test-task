package popfalushi.revolut;

import io.netty.handler.codec.http.HttpResponseStatus;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import popfalushi.revolut.business.moneyTransfer.MoneyTransferHandler;
import popfalushi.revolut.business.moneyTransfer.exception.MoneyTransferException;
import popfalushi.revolut.request.MoneyTransferRequest;
import popfalushi.revolut.request.RequestValidationException;

import java.util.UUID;

public class Main {
    static {
        System.getProperties().put("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
    }

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);
    private static final String SQL_CONNECTION_ATTR = "sqlConnection";
    private static final String REQUEST_GUID_ATTR = "requestGuid";

    private static void init2(JDBCClient jdbc) {
        LOG.info("Initializing db");
        jdbc.getConnection(conn -> {
            if (conn.failed()) {
                LOG.error("Failed initialization", conn.cause());
            }
            LOG.info("Got connection.");
            conn.result().execute("RUNSCRIPT FROM 'classpath:init.sql'", initScriptAsyncResult -> {
                if (initScriptAsyncResult.failed()) {
                    LOG.error("Failed initialization", conn.cause());
                } else {
                    LOG.info("InitializationComplete");
                }
                conn.result().close();
            });
        });
    }

    public static void main(String[] args) {
        // Create an HTTP server which simply returns "Hello World!" to each request.
        Vertx vertx = Vertx.vertx();
        JDBCClient jdbc = initJdbcConnection(vertx);
        HttpServer server = vertx.createHttpServer();
        Router router = Router.router(vertx);
        /**
         * incoming params:
         *  accountFrom: "string",
         *  accountTo: "string",
         *  amount: long
         */
        init2(jdbc);


        router.route().method(HttpMethod.POST).handler(BodyHandler.create());
        router.route().handler(routingContext -> {
            routingContext.put(REQUEST_GUID_ATTR, UUID.randomUUID().toString());
            jdbc.getConnection(sqlConnectionResult -> {
                if (sqlConnectionResult.succeeded()) {
                    routingContext.put(SQL_CONNECTION_ATTR, sqlConnectionResult.result());
                    routingContext.next();
                } else {
                    routingContext.fail(sqlConnectionResult.cause());
                }
            });
        });
        router.route().method(HttpMethod.POST).failureHandler(failureRoutingContext -> {
            SQLConnection sqlConnection = failureRoutingContext.get(SQL_CONNECTION_ATTR);
            if (sqlConnection != null) {
                sqlConnection.rollback(r -> {
                    LOG.debug("closing connection from failure");
                    sqlConnection.close();
                });
            }
            String requestGuid = failureRoutingContext.get(REQUEST_GUID_ATTR);
            Throwable cause = failureRoutingContext.failure();

            String msg = cause == null ? "nullcause" : cause.getMessage();
            LOG.error("Failed request with id: {}. Cause: {}", requestGuid, msg);
            if (cause instanceof MoneyTransferException) {
                HttpServerResponse response = failureRoutingContext.response();
                response.setStatusCode(HttpResponseStatus.BAD_REQUEST.code());
                response.setStatusMessage(msg);
                response.end();
            } else {
                HttpServerResponse response = failureRoutingContext.response();
                response.setStatusCode(HttpResponseStatus.INTERNAL_SERVER_ERROR.code());
                response.setStatusMessage(msg);
                response.end();
            }

        });

        router.route("/money-transfer")
                .method(HttpMethod.POST)
                .consumes("application/json")
//                .handler(BodyHandler.create())
                .handler(routingContext -> {
                    try {
                        String requestGuid = routingContext.get(REQUEST_GUID_ATTR);
                        SQLConnection sqlConnection = routingContext.get(SQL_CONNECTION_ATTR);
                        LOG.debug("money-transfer incoming request body: {}. id: {}", routingContext.getBodyAsString(), requestGuid);
                        MoneyTransferHandler moneyTransferHandler = new MoneyTransferHandler(sqlConnection, requestGuid);
                        JsonObject json = routingContext.getBodyAsJson(); //JSON is not necessary, but it is convenient nevertheless.
                        MoneyTransferRequest moneyTransferRequest = new MoneyTransferRequest(json.getString("accountFrom"), json.getString("accountTo"), json.getLong("amount"));
                        moneyTransferHandler.executeTransfer(moneyTransferRequest, routingContext, transferAsyncResult -> {
                            if (transferAsyncResult.succeeded()) {
                                LOG.debug("Finished successfully money transfer with id: {}. {}, {}, {}, {}",
                                        requestGuid,
                                        transferAsyncResult.result().from.getGuid(),
                                        transferAsyncResult.result().from.getAmount(),
                                        transferAsyncResult.result().to.getGuid(),
                                        transferAsyncResult.result().to.getAmount());
                                HttpServerResponse response = routingContext.response();
                                response.setStatusCode(HttpResponseStatus.OK.code());
                                response.end(transferAsyncResult.result().from.getAmount() + " " + transferAsyncResult.result().to.getAmount());
                                routingContext.next();
                            } else {
                                routingContext.fail(transferAsyncResult.cause());
                            }
                        });

                    } catch (RuntimeException e) {
                        LOG.error("Something wrong with handling request.", e);
                        routingContext.fail(e);
                    }
                });

        router.get("/account/:accountGuid").handler(routingContext -> {
            SQLConnection sqlConnection = routingContext.get(SQL_CONNECTION_ATTR);
            String accountGuid = routingContext.request().getParam("accountGuid");
            LOG.info("Getting amount for {}", accountGuid);
            sqlConnection.queryWithParams("SELECT amount FROM account WHERE guid=?", new JsonArray().add(accountGuid), queryResultAsync -> {
                Long amount = queryResultAsync.result().getRows().get(0).getLong("AMOUNT");
                HttpServerResponse response = routingContext.response();
//                response.putHeader("Content-Length", Integer.toString(amount.toString().getBytes().length));
//                response.write(amount.toString());
                response.setStatusCode(HttpResponseStatus.OK.code());
                response.end(amount.toString());
                routingContext.next();
            });
        });

        router.get("/accounts/sum").handler(routingContext -> {
            SQLConnection sqlConnection = routingContext.get(SQL_CONNECTION_ATTR);
            LOG.info("Getting sum(amount)");
            sqlConnection.query("SELECT sum(amount) FROM account",queryResultAsync -> {
                Long amount = queryResultAsync.result().getRows().get(0).getLong("SUM(AMOUNT)");
                HttpServerResponse response = routingContext.response();
                response.setStatusCode(HttpResponseStatus.OK.code());
                response.end(amount.toString());
                routingContext.next();
            });
        });

        router.route().handler(routingContext -> {
            SQLConnection sqlConnection = routingContext.get(SQL_CONNECTION_ATTR);
            if(sqlConnection != null){
                LOG.debug("closing connection");
                sqlConnection.close();
            }
        });

        server.requestHandler(router::accept).listen(8080);
    }

    private static JDBCClient initJdbcConnection(Vertx vertx) {
        JsonObject config = new JsonObject()
                .put("url", "jdbc:h2:mem:test;")
                .put("driver_class", "org.h2.Driver")
                .put("max_pool_size", 30);
        return JDBCClient.createNonShared(vertx, config);
    }

}