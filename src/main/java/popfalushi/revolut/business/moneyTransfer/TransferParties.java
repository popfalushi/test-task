package popfalushi.revolut.business.moneyTransfer;

import popfalushi.revolut.entities.Account;

/**
 *
 */
public class TransferParties {
    private TransferParties(Account from, Account to) {
        this.from = from;
        this.to = to;
    }
    public final Account from;
    public final Account to;

    public static TransferParties create(Account[] accounts, String from, String to){
        if(accounts == null || accounts.length != 2){
            throw new IllegalArgumentException();
        }
        if(accounts[0].getGuid().equals(from) && accounts[1].getGuid().equals(to)){
            return new TransferParties(accounts[0], accounts[1]);
        } else if (accounts[1].getGuid().equals(from) && accounts[0].getGuid().equals(to)){
            return new TransferParties(accounts[1], accounts[0]);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return "TransferParties{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}
