package popfalushi.revolut.business.moneyTransfer.exception;

/**
 *
 */
public class MoneyTransferException extends RuntimeException{
    public MoneyTransferException() {
    }

    public MoneyTransferException(String message) {
        super(message);
    }

    public MoneyTransferException(String message, Throwable cause) {
        super(message, cause);
    }

    public MoneyTransferException(Throwable cause) {
        super(cause);
    }

    public MoneyTransferException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
