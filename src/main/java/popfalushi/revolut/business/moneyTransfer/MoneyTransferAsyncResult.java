package popfalushi.revolut.business.moneyTransfer;

import io.vertx.core.AsyncResult;

/**
 *
 */
public class MoneyTransferAsyncResult implements AsyncResult<TransferParties> {
    private Throwable cause;
    private TransferParties transferParties;
    public MoneyTransferAsyncResult(Throwable cause) {
        this.cause = cause;
    }

    public MoneyTransferAsyncResult(TransferParties transferParties) {
        this.transferParties = transferParties;
    }

    @Override
    public TransferParties result() {
        return transferParties;
    }

    @Override
    public Throwable cause() {
        return cause;
    }

    @Override
    public boolean succeeded() {
        return cause == null;
    }

    @Override
    public boolean failed() {
        return !succeeded();
    }
}
