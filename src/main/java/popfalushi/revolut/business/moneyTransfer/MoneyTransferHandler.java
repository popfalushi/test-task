package popfalushi.revolut.business.moneyTransfer;

import io.netty.util.internal.StringUtil;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.RoutingContext;
import popfalushi.revolut.business.moneyTransfer.exception.NotEnoughMoneyException;
import popfalushi.revolut.entities.Account;
import popfalushi.revolut.request.MoneyTransferRequest;

import java.util.Date;
import java.util.List;

/**
 *
 */
public class MoneyTransferHandler {
    private final Logger LOG = LoggerFactory.getLogger(MoneyTransferHandler.class);
    private SQLConnection sqlConn;
    private final String SQL_UPDATE_ACCOUNTS = "UPDATE account SET " +
            "amount = CASE " +
            "WHEN guid=? THEN ? " +
            "WHEN guid=? THEN ? " +
            "END " +
            "where guid=? or guid=?";
    private final String SQL_INSERT_INTO_MONEY_TRANSFER_LOG = "INSERT INTO money_transfer_log(date, accountFrom, accountTo, amount) values (?, ?, ?, ?)";
    private final String SQL_SELECT_ACCOUNTS = "SELECT guid, amount FROM account WHERE guid = ? OR guid = ? FOR UPDATE";
    private String requestGuid = null;

    public MoneyTransferHandler(SQLConnection sqlConn, String requestGuid) {
        if (sqlConn == null) {
            throw new IllegalArgumentException("MoneyTransferHandler must be instantiated with sqlConn");
        }
        if (StringUtil.isNullOrEmpty(requestGuid)) {
            throw new IllegalArgumentException("MoneyTransferHandler must be instantiated with requestGuid");
        }
        this.sqlConn = sqlConn;
        this.requestGuid = requestGuid;
    }

    private Account createAccountFromJsonObjectRow(JsonObject jsonObject) {
        return new Account(jsonObject.getString("GUID"), jsonObject.getLong("AMOUNT"));
    }

    private TransferParties getTransferPartiesFromResultSet(List<JsonObject> resultSet, String accountFrom, String accountTo) {
        switch (resultSet.size()) {
            case 0:
                throw new IllegalArgumentException("No guids were found");
            case 1:
                throw new IllegalArgumentException("One guid was not found");
            case 2:
                Account account1 = createAccountFromJsonObjectRow(resultSet.get(0));
                Account account2 = createAccountFromJsonObjectRow(resultSet.get(1));
                return TransferParties.create(new Account[]{account1, account2}, accountFrom, accountTo);
            default:
                throw new IllegalArgumentException("Too many rows found: " + resultSet.size());
        }
    }

    private TransferParties getNewAccountsStates(List<JsonObject> accountsResultSet, MoneyTransferRequest moneyTransferRequest) throws NotEnoughMoneyException{
        TransferParties transferParties = getTransferPartiesFromResultSet(accountsResultSet, moneyTransferRequest.getAccountFrom(), moneyTransferRequest.getAccountTo());
        if (transferParties.from.getAmount() < moneyTransferRequest.getAmount()) {
            throw new NotEnoughMoneyException();
        }
        transferParties.from.setAmount(transferParties.from.getAmount() - moneyTransferRequest.getAmount());
        transferParties.to.setAmount(transferParties.to.getAmount() + moneyTransferRequest.getAmount());
        return transferParties;
    }
//
//    private void abort(SQLConnection sqlConn, AsyncResult failedStep){
//        abort(sqlConn, failedStep, "no_message");
//    }

    private void abort(Throwable cause, RoutingContext routingContext) {
        LOG.error(requestGuid + " Error occured while executing money transfer.", cause);
        routingContext.fail(cause);
    }

    public void executeTransfer(MoneyTransferRequest moneyTransferRequest, RoutingContext routingContext, Handler<AsyncResult<TransferParties>> handler) {
        JsonArray selectAccountsQueryParams = new JsonArray().add(moneyTransferRequest.getAccountFrom()).add(moneyTransferRequest.getAccountTo());
        sqlConn.queryWithParams(SQL_SELECT_ACCOUNTS, selectAccountsQueryParams, selectAccountsAsyncResult -> {
            if (selectAccountsAsyncResult.succeeded()) {
                try {
                    List<JsonObject> accountsResultSet = selectAccountsAsyncResult.result().getRows();
                    TransferParties transferParties = getNewAccountsStates(accountsResultSet, moneyTransferRequest);
                    JsonArray updateParam = new JsonArray()
                            .add(transferParties.from.getGuid())
                            .add(transferParties.from.getAmount())
                            .add(transferParties.to.getGuid())
                            .add(transferParties.to.getAmount())
                            .add(transferParties.from.getGuid())
                            .add(transferParties.to.getGuid());
                    sqlConn.updateWithParams(SQL_UPDATE_ACCOUNTS, updateParam, updateAccountsAsyncResult -> {
                        if (updateAccountsAsyncResult.succeeded()) {
                            JsonArray insertParams = new JsonArray()
                                    .add(new Date().toInstant().toString())
                                    .add(transferParties.from.getGuid())
                                    .add(transferParties.to.getGuid())
                                    .add(moneyTransferRequest.getAmount());
                            sqlConn.updateWithParams(SQL_INSERT_INTO_MONEY_TRANSFER_LOG, insertParams, insertLogAsyncResult -> {
                                if (insertLogAsyncResult.succeeded()) {
                                    sqlConn.commit(commitResult -> {
                                        sqlConn.close();
                                        LOG.debug("Committed money transfer.");
                                        handler.handle(new MoneyTransferAsyncResult(transferParties));
                                    });
                                } else {
                                    abort(insertLogAsyncResult.cause(), routingContext);
                                }
                            });
                        } else {
                            abort(updateAccountsAsyncResult.cause(), routingContext);
                        }
                    });
                } catch (Throwable t){
                    routingContext.fail(t);
                }
            } else {
                abort(selectAccountsAsyncResult.cause(), routingContext);
            }
        });
    }
}


