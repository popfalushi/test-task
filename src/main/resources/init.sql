create table if not exists account (
  guid VARCHAR(36) PRIMARY KEY ,
  amount number
);

create table if not exists money_transfer_log (
  date DATETIME,
  accountFrom VARCHAR(36),
  accountTo VARCHAR(36),
  amount number
  --TODO: foreign keys
);

INSERT into account(guid, amount) values ('1', 1000000000);
INSERT into account(guid, amount) values ('2', 1000000000);
