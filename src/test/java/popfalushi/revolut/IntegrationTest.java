package popfalushi.revolut;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 */
public class IntegrationTest {
    private static final Logger LOG = LoggerFactory.getLogger(IntegrationTest.class);
    static {
        System.getProperties().put("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory");
    }
    Thread t = new Thread(() -> Main.main(null));
    {
        t.start();
    }

    @Test
    public void testSumInvariant() throws InterruptedException {
        Vertx vertx = Vertx.vertx();
        Thread.sleep(3000);
        HttpClient httpClient = vertx.createHttpClient();
        for(int i = 0; i < 100000; i++){
            JsonObject jsonObject = new JsonObject();
            jsonObject.put("accountFrom", '1');
            jsonObject.put("accountTo", '2');
            jsonObject.put("amount", 1);
            HttpClientRequest request = httpClient.post(8080, "localhost", "/money-transfer", response -> {
                LOG.debug("Received response with status code " + response.statusCode());
            });
            request.putHeader("content-length", Integer.toString(jsonObject.encode().getBytes().length));
            request.putHeader("content-type", "application/json");
            request.write(jsonObject.encode());
            request.end();
        }
        Thread.sleep(20000L);
        LOG.info("Slept well");

        httpClient.get(8080, "localhost", "/accounts/sum", response -> {
            LOG.info("got GET response");
            response.bodyHandler(body -> {
                String bodyStr = body.toString();
                LOG.info("body: {}", bodyStr);
                Assert.assertEquals(bodyStr, "2000000000");
            });
        }).end();
        Thread.sleep(2000L);

    }
}
